package com.alipour.notes.controller;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

/**
 * Created by alipour on 10/11/16.
 */
public class MainController {
    public VBox content;


    public void tags(ActionEvent e) {
        final TagsController controller = new TagsController();
        content.getChildren().add(controller.getNode());

    }
}
