package com.alipour.notes.controller;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import java.io.IOException;

/**
 * Created by alipour on 10/12/16.
 */
public class Controller {

    public Node getNode(){
        Label label = new Label("This action not implement yet!", new ImageView("com/alipour/notes/view/images/under_construction.png"));
        label.setAlignment(Pos.CENTER);
        return label;
    }

    protected Node getView(String uri){
        try {
            return FXMLLoader.load(getClass().getResource(uri));
        } catch (IOException e) {
            return getNode();
        }
    }
}
