package com.alipour.notes.controller;

import com.alipour.notes.model.Note;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by alipour on 10/12/16.
 */
public class TagsController extends Controller implements Initializable{
    public ComboBox comboBoxType;
    public TextField txtTitle;
    public ComboBox<Note> comboBoxNote;

    @Override
    public Node getNode() {
        return getView("../view/tags.fxml");
    }

    public void search(ActionEvent actionEvent) {
        System.out.println(comboBoxNote.getSelectionModel().getSelectedItem().getId());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final ObservableList<String> data = FXCollections.observableArrayList(
                "Jacob",
                "Isabella", "Johnson", "isabella.johnson@example.com",
                "Ethan", "Williams", "ethan.williams@example.com",
                "Emma", "Jones", "emma.jones@example.com",
                "Michael", "Brown", "michael.brown@example.com"
        );
        comboBoxType.setItems(data);

        Note note = new Note();
        note.setId(12L);
        note.setTitle("test");
        comboBoxNote.setItems(FXCollections.observableArrayList(note));
    }
}
