package com.alipour.notes.model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by alipour on 10/12/16.
 */
public class Notification extends ParentEntity {
    private Note note;
    private Status status;
    private LocalDate date;
    private LocalTime time;

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
