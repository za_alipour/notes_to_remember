package com.alipour.notes.model;

/**
 * Created by alipour on 10/12/16.
 */
public class ParentEntity {
    private Long id;

    public ParentEntity() {
    }

    public ParentEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
