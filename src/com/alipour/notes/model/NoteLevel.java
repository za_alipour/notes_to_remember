package com.alipour.notes.model;

/**
 * Created by alipour on 10/12/16.
 */
public class NoteLevel extends ParentEntity{
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
