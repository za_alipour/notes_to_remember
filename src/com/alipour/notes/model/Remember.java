package com.alipour.notes.model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by alipour on 10/12/16.
 */
public class Remember extends ParentEntity{
    private Note note;
    private LocalDate date;
    private LocalTime time;
    private Boolean done;

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
