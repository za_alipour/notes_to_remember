package com.alipour.notes.model;

/**
 * Created by alipour on 10/12/16.
 */
public class Tag {
    private String title;
    private Boolean systemic;
    private Note note;

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Boolean getSystemic() {
        return systemic;
    }

    public void setSystemic(Boolean systemic) {
        this.systemic = systemic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
