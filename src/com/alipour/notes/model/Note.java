package com.alipour.notes.model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by alipour on 10/12/16.
 */
public class Note extends ParentEntity {
    private String title;
    private String description;
    private LocalDate date;
    private LocalTime time;
    private NoteLevel level;
    private Notification notification;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public NoteLevel getLevel() {
        return level;
    }

    public void setLevel(NoteLevel level) {
        this.level = level;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return title;
    }
}
