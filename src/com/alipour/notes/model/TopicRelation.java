package com.alipour.notes.model;

/**
 * Created by alipour on 10/12/16.
 */
public class TopicRelation extends ParentEntity {
    private Topic fromTopic;
    private Topic toTopic;

    public Topic getFromTopic() {
        return fromTopic;
    }

    public void setFromTopic(Topic fromTopic) {
        this.fromTopic = fromTopic;
    }

    public Topic getToTopic() {
        return toTopic;
    }

    public void setToTopic(Topic toTopic) {
        this.toTopic = toTopic;
    }
}
