package com.alipour.notes.model;

import java.sql.Timestamp;

/**
 * Created by alipour on 10/12/16.
 */
public class Topic extends ParentEntity{
    private String title;
    private Timestamp createTime;
    private Boolean active;
    private Category category;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
